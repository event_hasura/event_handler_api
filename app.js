const express = require('express');
const cors = require('cors');
// const base64Img = require('base64-img')
require('dotenv').config({ path: '.env' });;

const googleHandler = require('./actionHandler/googlelogin');
const signupHandler = require('./actionHandler/signUpHandler');
const imageUpload = require('./actionHandler/imageUpload');
const multImageUp = require('./actionHandler/multiple_pic')
const mapHandler = require('./actionHandler/map');
const login_hook_handler = require('./actionHandler/users/login_hook');
// const loginHandler = require('./actionHandler/loginHandler');




const app = express();
// app.use(bodyParser.json());
app.use(express.json({ limit: '50mb' }));// Body parser Middleware
app.use(cors());
app.use(express.static('./public'));
console.log("Hey There!")


app.post('/googlelogin', googleHandler);
app.post('/signup', signupHandler);
app.post('/imageUpload', imageUpload);
app.post('/multi_upload', multImageUp);
app.post('/getCoder', mapHandler);
app.post('/login_hook', login_hook_handler)


const port = process.env.EXPRESS_PORT || 5000;
app.listen(port, () => {
    console.log(`App is running on ${port}`)
});