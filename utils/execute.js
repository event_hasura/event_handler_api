const fetch = require('node-fetch')

const execute = async (variables, reqHeaders, operation) => {
    const fetchRespose = await fetch(
        "http://localhost:8080/v1/graphql",
        {
            method: 'POST',
            headers: reqHeaders || {},
            body: JSON.stringify({
                query: operation,
                variables
            }),
        }
    )
    const response = await fetchRespose.json();
    return response;
}

module.exports = execute;

