FROM node:lts-alpine3.9

WORKDIR /usr/src/app    

ARG EXPRESS_PORT

EXPOSE ${EXPRESS_PORT}

RUN apk add npm

COPY package.json package-lock.json ./

RUN npm install

COPY app.js .

# COPY [all but **/node_modules/**] .

COPY . .

CMD ["npm", "start"]