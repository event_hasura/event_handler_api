const fetch = require('node-fetch')
const jwt = require('jsonwebtoken')
const bcrypt = require('bcrypt')

// set mutation op_ to send a new user to graphQL

const HASURA_OPERATION = `
        mutation($username: String!, $password: String!){
            insert_user_one (object: {
                username: $username,
                password: $password
            }){
                id
            }
        }
`

// Execute the parent mutation in hasura
const execute = async (variables, reqHeaders) => {
    const fetchRespose = await fetch(
        "http://localhost:8080/v1/graphql",
        {
            method: 'POST',
            headers: reqHeaders || {},
            body: JSON.stringify({
                query: HASURA_OPERATION,
                variables
            })
        }
    )
    return await fetchRespose.json();
}

const handleSignup = async (req, res) => {

    // get credentials / data from user
    const { username, password } = req.body.input

    // Hash user password 
    const hashedPassword = await bcrypt.hash(password, 12);
    // Execute the hasura operation
    const { data, error } = await execute({ username, password: hashedPassword }, req.headers);

    // If there is an error in hasura operation , throw error ...
    if (error) {
        console.log(error)
        return res.status(400).json({
            Message: error
        })
    }

    // Now, we gonna sign the token based on the information provided
    const token_content = {
        sub: { ...data.insert_user_one }.id.toString(),
        name: username,
        iat: Date.now() / 1000,
        "https://hasura.io/jwt/claims": {
            "x-hasura-allowed-role": ["user"],
            "x-hasura-user-id": { ...data.insert_user_one }.id.toString(),
            "x-hasura-default-role": "user",
            "x-hasura-role": "user"
        },
        exp: Math.floor(Date.now() / 1000) + (60 * 60) // Token duration
    };

    //   sign it based on content
    const token = jwt.sign(token_content, process.env.JWT_SECRET_KEY);


    console.log(data)
    return res.json({
        ...data.insert_user_one,
        username,
        password: hashedPassword,
        token

    })
}

module.exports = handleSignup;