
const fetch = require('node-fetch')
const jwt = require('jsonwebtoken')
const bcrypt = require('bcrypt')
// const { text } = require('body-parser')

const HASURA_OPERATION = `
        query ($username: String!, $password: String!){
            user( where: {username: {_eq: $username } }) {
                id,
                username,
                password,
                role
            }
        }
`

// Execute parent query
const execute = async (variables, reqHeaders) => {
    const fetchRespose = await fetch(
        "http://localhost:8080/v1/graphql",
        {
            method: 'POST',
            headers: reqHeaders || {},
            body: JSON.stringify({
                query: HASURA_OPERATION,
                variables
            }),
        }
    )
    return await fetchRespose.json();
}

// Compare password 
const comparePassword = async (textPassword, candidatePassword) => {
    return await bcrypt.compare(textPassword, candidatePassword);
}


const handleLogin = async (req, res) => {
    try {

        const { username, password } = req.body.input;
        const variables = { username, password }
        console.log(variables)
        // Check if user found in DB
        const { data, error } = await execute(variables, req.headers);

        if (typeof (data) == 'undefined') {
            return res.status(404).json({
                message: 'User is not defined'
            })
        }
        // Check if error occured in hasura operaion

        if (error) {
            console.log("ERROR-OCCURED", error);
            return res.status(404).json({
                message: error
            })
        }


        console.log(data)
        const { id, role } = data.user[0];
        const candidatePassword = data.user[0].password;

        console.log(id, candidatePassword)
        // Check if the password provided is correct with from DB
        const compare = await comparePassword(password, candidatePassword);

        if (!compare) {
            return res.status(404).json({
                message: "Incorrect password"
            })
        }
        // sign token 
        const token_content = {
            sub: id.toString(),
            name: username,
            iat: Date.now() / 1000,
            "https://hasura.io/jwt/claims": {
                "x-hasura-allowed-roles": ["user"],
                "x-hasura-user-id": id.toString(),
                "x-hasura-default-role": "user",
                "x-hasura-role": role
            },
            exp: Math.floor(Date.now() / 1000) + (60 * 60)
        };

        // Now, sign token based on token content
        const token = jwt.sign(token_content, process.env.JWT_SECRET_KEY);
        // Else return desirable result to the client
        return res.json({
            id,
            username,
            token
        })

    } catch (e) {
        console.log(e.message);
        return res.status(404).json({
            message: e.message
        })
    }
}

module.exports = handleLogin;