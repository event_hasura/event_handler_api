const gql = require("graphql-tag");
const apollo_client = require("../apollo");

const login_hook = async (req, res) => {
    try { 
        const { user } = req.body;
        console.log({user})
        let result = await apollo_client.mutate({
            mutation: gql`
            mutation($id: String!, $email: String!, $username: String!){
                insert_event_users_one(object: {id: $id, email: $email, username: $username},
                    on_conflict: {constraint: users_event_pkey,
                    update_columns: email,}) {
                    email
                    id
                    username
                    role
                    path
                }
            }
            `, 
            variables: {
                    id: user.id,
                    // phone_number: user.phone_number,
                    email: user.email,
                    username: user.name,
                    // created_by: user.id,
            }
        });

        console.log("From my DB");
        console.log(result)

        let { data } = result;

        // data.insert__user.roles.push("user");

        res.send({
            "x-hasura-user-id": data.insert_event_users_one.id,
            "x-hasura-allowed-roles": ["admin","user", "organizer"],
            "x-hasura-default-role": "user",
            "x-hasura-role": data.insert_event_users_one.role,
            "x-hasura-path": data.insert_event_users_one.path?data.insert_event_users_one.path: "",
            "x-hasura-name": data.insert_event_users_one.username
        });

    } catch (error) {
        console.error(error);
        throw error;
    }
}


module.exports = login_hook;