// const base64Img = require('base64-img')
const cloudinary = require("cloudinary").v2;
const gql = require("graphql-tag");
const apollo_client = require("./apollo");

// const upload_image = `
//        mutation($objects:[images_insert_input!]!){
//         insert_images(objects: $objects) {
//             returning {
//             eventId
//             id
//             path
//             userId
//             }
//         }
//        }
//    `;
//    const insert_img = gql`
//               mutation($eventId: Int!, $userId: String!, $path: String!){
//                 insert_images_one(object: {eventId: $eventId, userId: $userId, path: $path}) {
//                     id
//                     path
//                 }
//             }
//             `

const imageUpload = async (req, res) => {

    const { image } = req.body.input;
    try {
        // const { userId, eventId } = req.body.input
        // console.log(userId, eventId)
        console.log(image.length)

        // const { image, folder } = req.body.input;

        let secure_urls = [];

        let urls = [];

        for (let file in image) {

            file = image[file];

            let result = await cloudinary.uploader.upload(file, {
                        unique_filename: true,
                        discard_original_filename: true,
                        timeout: 120000
                    });

            secure_urls.push(result.secure_url);

            // urls.push(result.url)

            urls.push({
                path: result.url,
            });
        }
        
        console.log(urls)

         res.status(200).json({
            data: urls
        })

    }
    catch (error) {
        console.log("The error is - " , error)
        // res.status(500).send({
        //     message: "Error Uploading Files",
        //     code: "error_uploading_file"
        // });
    }
}

module.exports = imageUpload