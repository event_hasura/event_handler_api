const fetch = require('node-fetch')

async function sendGeocodingRequest(location) {
    let response = await fetch(`https://api.traveltimeapp.com/v4/geocoding/search?query=` + location, {
        method: "GET",
        credentials: "same-origin",
        headers: {
            "Content-Type": "application/json",
            "X-Application-Id": "b6b1f19d",
            "X-Api-Key": "40e1987fc1487efa5e3ddc3c1e93c882",
            "Accept-Language": "en-US"
        }

    });
    // .then(response => response.json());
    return await response.json();
}

const mapHandler = async (req, res) => {
    console.log(req.body.input);
    // console.log(req.body.input)
    let response = await sendGeocodingRequest(req.body.input.location);
    // console.log(response.features[0])
    // console.log(response.features[0].geometry.coordinates[0])
    let lat = response.features[0].geometry.coordinates[1]
    let lng = response.features[0].geometry.coordinates[0]
    // console.log(lat)
    // console.log(lng)
    res.status(200).json({
         lat: lat,
         lng: lng
    })

}

module.exports = mapHandler;